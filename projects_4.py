# -*- coding: utf-8 -*-
"""Projects_4.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1gzwcwxnxyF0WJyeotrUt71irl_hoj1Md
"""

cd '/content/drive/MyDrive/Project_dataset'

ls

pwd

import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

from tensorflow import keras
from keras import regularizers, optimizers
from tensorflow.keras import Sequential
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, Dropout, Flatten, MaxPooling2D, Reshape, Conv2DTranspose, UpSampling2D, Input
from tensorflow.keras.preprocessing import image
from tensorflow.keras.optimizers import SGD
from tqdm import tqdm

from sklearn.model_selection import train_test_split
from  tensorflow.keras.callbacks import EarlyStopping

help(Conv2D)

"""#Load Lables CSV Collumns and Rows For Images"""

data_labels=pd.read_csv('labels_dataset.csv')

data_labels.shape

data_labels.columns

data_labels.head()

data_labels.shape[0]

import cv2 
img = cv2.imread('/content/drive/MyDrive/Project_dataset/images/1.jpg')

img.max()

img.min()

"""# Image Format for 2000 Images
1. Format Images 100pix by 100pix
2. Format images flowing labels  
2. Format from 255 = 0 to 1 images values
3. Store each processed images to array
"""

img_width = 100;
img_height = 100;
X =[]
for i in tqdm(range(data_labels.shape[0])):
  path = '/content/drive/MyDrive/Project_dataset/images/' + data_labels['Filenames'][i]
  img = image.load_img(path, target_size=(img_width, img_height, 3))
  img = image.img_to_array(img)
  img = img/255.0
  X.append(img)

X = np.array(X)

X.shape

X.max()

X.min()

plt.imshow(X[0])

print(X[8])

"""# Format Labels by removing Filenames column and creating a Label array"""

y = data_labels.drop(['Filenames'], axis = 1)
y = y.to_numpy()
y.shape

import sys
import numpy
numpy.set_printoptions(threshold=sys.maxsize)

y

"""# Splitting Training and Testing data
1. X_train = train images, y_train = labels for images, 80% of images 
2. X_test = test images, y_test = labels for images, 20% of images 
random_state seed. same random split every time
"""

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state = 0, test_size =0.20)

plt.imshow(X_train[0])

y_train[0]

class_names=["desert", "mountains", "sea", "sunset", "trees"]

y_train

plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(X_train1[i], cmap=plt.cm.binary)
    plt.xlabel(y_train1[i])
plt.show()

X_train.shape

y_train.shape

X_test.shape

y_test.shape

y_train.dtype

class_names=["desert", "mountains", "sea", "sunset", "trees"]

#model.fit(x_train, y_train, epochs=30, validation_data=(x_valid, y_valid))

"""# Model 1"""

model = Sequential()
model.add(Conv2D(32, (3, 3), padding='same',input_shape=(100,100,3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(Dense(5, activation='sigmoid'))
model.compile( loss="binary_crossentropy",metrics=["accuracy"], optimizer='adam')

model.summary()

model.fit(X_train, y_train, epochs=10, validation_data= (X_test,y_test))

#predictions = model.predict(X_test)

"""# Model 2"""

X_train1, X_test1, y_train1, y_test1 = train_test_split(X, y, random_state = 0, test_size =0.20)

model1 = Sequential()
model1.add(Conv2D(32, (3, 3), padding='same',input_shape=(100,100,3), activation='relu'))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Dropout(0.25))
model1.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model1.add(MaxPooling2D(pool_size=(2, 2)))
model1.add(Dropout(0.25))



model1.add(Flatten())
model1.add(Dense(512, activation='relu'))


model1.add(Dense(5, activation='sigmoid'))


model1.compile( loss="binary_crossentropy",metrics=["accuracy"], optimizer='adam')

model1.summary()

early_stop = EarlyStopping(monitor='val_loss', patience=1)
callbacks=[early_stop]

model1.fit(X_train1, y_train1, epochs=10, validation_data= (X_test1,y_test1), callbacks=[early_stop])

"""# Predict"""

input_img1 = image.load_img('/content/a.jpg', target_size=(img_width, img_height, 3))
plt.imshow(input_img1);
input_img1 = image.img_to_array(input_img1)
input_img1 = input_img1/255.0
input_img1 = input_img1.reshape(1, img_width, img_height, 3)

predictions_single1 = model1.predict(input_img1)
class_names=["desert", "mountains", "sea", "sunset", "trees"]
print(predictions_single1)

input_img2 = image.load_img('/content/b.jpg', target_size=(img_width, img_height, 3))
plt.imshow(input_img2);
input_img2 = image.img_to_array(input_img2)
input_img2 = input_img2/255.0
input_img2 = input_img2.reshape(1, img_width, img_height, 3)

predictions_single2 = model1.predict(input_img2)
class_names=["desert", "mountains", "sea", "sunset", "trees"]
print(predictions_single2)

model1.evaluate(X_test1, y_test1)

x_new = X_test1[:4]
y_proba = model1.predict(x_new)
y_proba.round(2)

a_test = np.argsort(y_proba[0])[:-3:-1]
a_test

classes = data_labels.columns[1:]
classes

for i in range(2):
  print(classes[top[i]])

plt.imshow(X_test[0])

plt.imshow(X_test[1])

plt.imshow(X_test[2])

plt.imshow(X_test[3])

predictions = model1.predict(input_img2)
predictions

top = np.argsort(predictions[0])[:-3:-1]
top

classes = data_labels.columns[1:]
classes

for i in range(2):
  print(classes[top[i]])

"""# AUTO ENCODER"""

input_layer = Input(shape=(100,100,3), name="INPUT")
x = Conv2D(32, (3,3), activation='relu', padding='same')(input_layer)
x = MaxPooling2D((2,2))(x)
x = Conv2D(64, (3,3), activation='relu', padding='same')(x)

code_layer = MaxPooling2D((2,2),name="CODE")(x)

x = Conv2DTranspose(64, (3,3), activation='relu', padding='same')(code_layer)
x = UpSampling2D((2,2))(x)
x = Conv2DTranspose(32, (3,3), activation='relu', padding='same')(x)
x = UpSampling2D((2,2))(x)
output_layer = Conv2D(3,(3,3), padding='same', name="OUTPUT")(x)

ae_test1 = Model(input_layer, output_layer)
ae_test1.compile(optimizer='adam', loss='mse')
ae_test1.summary()

ae_test1.fit(X_train1, X_train1,epochs=10, validation_data=(X_test1, X_test1))

get_encoded = Model(inputs=ae_test1.input, outputs=ae_test1.get_layer("CODE").output)

enc = get_encoded.predict(X_test1)
#enc = enc.reshape((len(X_test1), 32*64*64))
enc.shape

reconstructed = ae_test1.predict(X_test1)

show_a()

plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(reconstructed[i], cmap=plt.cm.binary)
    plt.xlabel(y_train1[i])
plt.show()
const $a = selector => document.querySelector(selector);
let base64Image;
// gets the image from html file and stores that image as base64 in a variable
function previewFile() {
    const preview1 = $a('#a1');
    const preview2= $a('#a2');
    const preview3 = $a('#a3');
    const preview4 = $a('#a4');
    const preview5 = $a('#a5');
    const file = document.querySelector('input[type=file]').files[0];
    const fileReader = new FileReader();
    //console.log(preview);
    console.log(file);
    console.log(fileReader);
    fileReader.addEventListener("load", function () {
        if (file.type.match('image/jpeg')) {
            base64Image = fileReader.result.replace("data:image/jpeg;base64,", "");
            console.log("my jpeg");
            preview1.src = fileReader.result;
            preview2.src = fileReader.result;
            preview3.src = fileReader.result;
            preview4.src = fileReader.result;
            preview5.src = fileReader.result;
            picture = fileReader.result;
        } else if (file.type.match('image/png')) {
            base64Image = fileReader.result.replace("data:image/png;base64,", "");
            console.log("my png");
            preview1.src = fileReader.result;
            preview2.src = fileReader.result;
            preview3.src = fileReader.result;
            preview4.src = fileReader.result;
            preview5.src = fileReader.result;
        } else {
            console.log("invalid");
        };
    });
    if (file) {
        fileReader.readAsDataURL(file);
    };
    $a('#desert1').innerHTML = "Desert:%";
    $a('#mountains1').innerHTML = "Mountains:%";
    $a('#sea1').innerHTML = "Sea:%";
    $a('#sunset1').innerHTML = "Sunset:%";
    $a('#trees1').innerHTML = "Trees:%";
    
    $a('#desert2').innerHTML = "Desert:%";
    $a('#mountains2').innerHTML = "Mountains:%";
    $a('#sea2').innerHTML = "Sea:%";
    $a('#sunset2').innerHTML = "Sunset:%";
    $a('#trees2').innerHTML = "Trees:%";
    
    $a('#desert3').innerHTML = "Desert:%";
    $a('#mountains3').innerHTML = "Mountains:%";
    $a('#sea3').innerHTML = "Sea:%";
    $a('#sunset3').innerHTML = "Sunset:%";
    $a('#trees3').innerHTML = "Trees:%";

    $a('#desert4').innerHTML = "Desert:%";
    $a('#mountains4').innerHTML = "Mountains:%";
    $a('#sea4').innerHTML = "Sea:%";
    $a('#sunset4').innerHTML = "Sunset:%";
    $a('#trees4').innerHTML = "Trees:%";

    $a('#desert5').innerHTML = "Desert:%";
    $a('#mountains5').innerHTML = "Mountains:%";
    $a('#sea5').innerHTML = "Sea:%";
    $a('#sunset5').innerHTML = "Sunset:%";
    $a('#trees5').innerHTML = "Trees:%";


    $a("#desert1").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#mountains1").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sea1").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sunset1").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#trees1").style.backgroundColor = "rgb(185, 255, 195)";

    $a("#desert2").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#mountains2").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sea2").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sunset2").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#trees2").style.backgroundColor = "rgb(185, 255, 195)";

    $a("#desert3").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#mountains3").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sea3").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sunset3").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#trees3").style.backgroundColor = "rgb(185, 255, 195)";

    $a("#desert4").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#mountains4").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sea4").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sunset4").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#trees4").style.backgroundColor = "rgb(185, 255, 195)";

    $a("#desert5").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#mountains5").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sea5").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sunset5").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#trees5").style.backgroundColor = "rgb(185, 255, 195)";
};

// sends json image to /predict url and gets a response from the flask server. 
var predict = function () {
    var message = {image: base64Image};

    if (message.image == undefined) {
        console.log("invalid file format");
        return;
    };
    console.log("predictModel1");
    $.post("/predictModel1", JSON.stringify(message),
        function (response) {
            console.log("Reply from server try");
            console.log(response);
            let desert = response.prediction.desert.toFixed(2);
            let mountains = response.prediction.mountains.toFixed(2);
            let sea = response.prediction.sea.toFixed(2);
            let sunset = response.prediction.sunset.toFixed(2);
            let trees = response.prediction.trees.toFixed(2);
            $a("#desert1").innerHTML = "Desert: " + desert + "%";
            $a("#mountains1").innerHTML = "Mountains: " + mountains + "%";
            $a("#sea1").innerHTML = "Sea: " + sea + "%";
            $a("#sunset1").innerHTML = "Sunset: " + sunset + "%";
            $a("#trees1").innerHTML = "Trees: " + trees + "%";
            
            if (response.prediction.desert > 0.50) {
                $a("#desert1").style.backgroundColor = "pink";
            };
            if (response.prediction.mountains > 0.50) {
                $a("#mountains1").style.backgroundColor = "pink";
            };
            if (response.prediction.sea > 0.50) {
                $a("#sea1").style.backgroundColor = "pink";
            };
            if (response.prediction.sunset > 0.50) {
                $a("#sunset1").style.backgroundColor = "pink";
            };
            if (response.prediction.trees > 0.50) {
                $a("#trees1").style.backgroundColor = "pink";
            };
        });

    console.log("predictModel2");
    $.post("/predictModel2", JSON.stringify(message),
        function (response) {
            console.log("Reply from server try");
            console.log(response);
            let desert = response.prediction.desert.toFixed(2);
            let mountains = response.prediction.mountains.toFixed(2);
            let sea = response.prediction.sea.toFixed(2);
            let sunset = response.prediction.sunset.toFixed(2);
            let trees = response.prediction.trees.toFixed(2);
            $a("#desert2").innerHTML = "Desert: " + desert + "%";
            $a("#mountains2").innerHTML = "Mountains: " + mountains + "%";
            $a("#sea2").innerHTML = "Sea: " + sea + "%";
            $a("#sunset2").innerHTML = "Sunset: " + sunset + "%";
            $a("#trees2").innerHTML = "Trees: " + trees + "%";

            
            if (response.prediction.desert > 0.50) {
                $a("#desert2").style.backgroundColor = "pink";
            };
            if (response.prediction.mountains > 0.50) {
                $a("#mountains2").style.backgroundColor = "pink";
            };
            if (response.prediction.sea > 0.50) {
                $a("#sea2").style.backgroundColor = "pink";
            };
            if (response.prediction.sunset > 0.50) {
                $a("#sunset2").style.backgroundColor = "pink";
            };
            if (response.prediction.trees > 0.50) {
                $a("#trees2").style.backgroundColor = "pink";
            };
        });

        console.log("predictModel3");
        $.post("/predictModel3", JSON.stringify(message),
            function (response) {
                console.log("Reply from server try");
                console.log(response);
                let desert = response.prediction.desert.toFixed(2);
                let mountains = response.prediction.mountains.toFixed(2);
                let sea = response.prediction.sea.toFixed(2);
                let sunset = response.prediction.sunset.toFixed(2);
                let trees = response.prediction.trees.toFixed(2);
                $a("#desert3").innerHTML = "Desert: " + desert + "%";
                $a("#mountains3").innerHTML = "Mountains: " + mountains + "%";
                $a("#sea3").innerHTML = "Sea: " + sea + "%";
                $a("#sunset3").innerHTML = "Sunset: " + sunset + "%";
                $a("#trees3").innerHTML = "Trees: " + trees + "%";

                
                if (response.prediction.desert > 0.50) {
                    $a("#desert3").style.backgroundColor = "pink";
                };
                if (response.prediction.mountains > 0.50) {
                    $a("#mountains3").style.backgroundColor = "pink";
                };
                if (response.prediction.sea > 0.50) {
                    $a("#sea3").style.backgroundColor = "pink";
                };
                if (response.prediction.sunset > 0.50) {
                    $a("#sunset3").style.backgroundColor = "pink";
                };
                if (response.prediction.trees > 0.50) {
                    $a("#trees3").style.backgroundColor = "pink";
                };
            });

            console.log("predictModel4");
            $.post("/predictModel4", JSON.stringify(message),
                function (response) {
                    console.log("Reply from server try");
                    console.log(response);
                    let desert = response.prediction.desert.toFixed(2);
                    let mountains = response.prediction.mountains.toFixed(2);
                    let sea = response.prediction.sea.toFixed(2);
                    let sunset = response.prediction.sunset.toFixed(2);
                    let trees = response.prediction.trees.toFixed(2);
                    $a("#desert4").innerHTML = "Desert: " + desert + "%";
                    $a("#mountains4").innerHTML = "Mountains: " + mountains + "%";
                    $a("#sea4").innerHTML = "Sea: " + sea + "%";
                    $a("#sunset4").innerHTML = "Sunset: " + sunset + "%";
                    $a("#trees4").innerHTML = "Trees: " + trees + "%";

                    
                    if (response.prediction.desert > 0.50) {
                        $a("#desert4").style.backgroundColor = "pink";
                    };
                    if (response.prediction.mountains > 0.50) {
                        $a("#mountains4").style.backgroundColor = "pink";
                    };
                    if (response.prediction.sea > 0.50) {
                        $a("#sea4").style.backgroundColor = "pink";
                    };
                    if (response.prediction.sunset > 0.50) {
                        $a("#sunset4").style.backgroundColor = "pink";
                    };
                    if (response.prediction.trees > 0.50) {
                        $a("#trees4").style.backgroundColor = "pink";
                    };
                });

                
            console.log("predictModel5");
            $.post("/predictModel5", JSON.stringify(message),
                function (response) {
                    console.log("Reply from server try");
                    console.log(response);
                    let desert = response.prediction.desert.toFixed(2);
                    let mountains = response.prediction.mountains.toFixed(2);
                    let sea = response.prediction.sea.toFixed(2);
                    let sunset = response.prediction.sunset.toFixed(2);
                    let trees = response.prediction.trees.toFixed(2);
                    $a("#desert5").innerHTML = "Desert: " + desert + "%";
                    $a("#mountains5").innerHTML = "Mountains: " + mountains + "%";
                    $a("#sea5").innerHTML = "Sea: " + sea + "%";
                    $a("#sunset5").innerHTML = "Sunset: " + sunset + "%";
                    $a("#trees5").innerHTML = "Trees: " + trees + "%";

                    if (response.prediction.desert > 0.50) {
                        $a("#desert5").style.backgroundColor = "pink";
                    };
                    if (response.prediction.mountains > 0.50) {
                        $a("#mountains5").style.backgroundColor = "pink";
                    };
                    if (response.prediction.sea > 0.50) {
                        $a("#sea5").style.backgroundColor = "pink";
                    };
                    if (response.prediction.sunset > 0.50) {
                        $a("#sunset5").style.backgroundColor = "pink";
                    };
                    if (response.prediction.trees > 0.50) {
                        $a("#trees5").style.backgroundColor = "pink";
                    };
                });





    console.log("Reply from server end");
};


function myFunction() {
    var c = $(".histresults").children;
    var i;
    for (i = 0; i < c.length; i++) {
        console.log(i);
        $a(".desert").style.backgroundColor = "pink";
        $a(".trees").style.backgroundColor = "pink";
    };
};



window.onload = function () {
    $a("#predict_button").onclick = predict;
};
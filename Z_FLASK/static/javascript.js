const $a = selector => document.querySelector(selector);
let base64Image;


// gets the image from html file and stores that image as base64 in a variable
function previewFile() {
    const preview = $a('#selected_image');
    const file = document.querySelector('input[type=file]').files[0];
    const fileReader = new FileReader();
    console.log(preview);
    console.log(file);
    console.log(fileReader);
    fileReader.addEventListener("load", function () {
        if (file.type.match('image/jpeg')) {
            base64Image = fileReader.result.replace("data:image/jpeg;base64,", "");
            console.log("my jpeg");
            preview.src = fileReader.result;
            picture = fileReader.result;
        } else if (file.type.match('image/png')) {
            base64Image = fileReader.result.replace("data:image/png;base64,", "");
            console.log("my png");
            preview.src = fileReader.result;
        } else {
            console.log("invalid");

        };
    });

    if (file) {
        fileReader.readAsDataURL(file);
    };
    $a('#desert').innerHTML = "Desert:%";
    $a('#mountains').innerHTML = "Mountains:%";
    $a('#sea').innerHTML = "Sea:%";
    $a('#sunset').innerHTML = "Sunset:%";
    $a('#trees').innerHTML = "Trees:%";
    $a("#desert").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#mountains").style.backgroundColor = "rgb(185, 255, 195)";
    $a("#sea").style.backgroundColor = "rgb(185, 255, 195)";
    $a('#sunset').style.backgroundColor = "rgb(185, 255, 195)";
    $a("#trees").style.backgroundColor = "rgb(185, 255, 195)";
};

// sends json image to /predict url and gets a response from the flask server. 
var predict = function () {
    var message = {image: base64Image};

    if (message.image == undefined) {
        console.log("invalid file format");
        return;
    };

    console.log(message);
    console.log("predictModel1");
    $.post("/predictModel1", JSON.stringify(message),
        function (response) {
            console.log("Reply from server try");
            console.log(response);
            let desert = response.prediction.desert.toFixed(2);
            let mountains = response.prediction.mountains.toFixed(2);
            let sea = response.prediction.sea.toFixed(2);
            let sunset = response.prediction.sunset.toFixed(2);
            let trees = response.prediction.trees.toFixed(2);
            $a("#desert").innerHTML = "Desert: " + desert + "%";
            $a("#mountains").innerHTML = "Mountains: " + mountains + "%";
            $a("#sea").innerHTML = "Sea: " + sea + "%";
            $a("#sunset").innerHTML = "Sunset: " + sunset + "%";
            $a("#trees").innerHTML = "Trees: " + trees + "%";

            if (response.prediction.desert > 0.50) {
                $a("#desert").style.backgroundColor = "pink";
            };
            if (response.prediction.mountains > 0.50) {
                $a("#mountains").style.backgroundColor = "pink";
            };
            if (response.prediction.sea > 0.50) {
                $a("#sea").style.backgroundColor = "pink";
            };
            if (response.prediction.sunset > 0.50) {
                $a("#sunset").style.backgroundColor = "pink";
            };
            if (response.prediction.trees > 0.50) {
                $a("#trees").style.backgroundColor = "pink";
            };


            var html = "<div class='test'>";
            html += "<div id='histimg'>"
            html += "<img class='img_history' src='" + picture + "'/>";
            html += "</div>"
            html += "<div class='histresults'>"
            html += "<p class='desert'  id='desert'>Desert: " + desert + "%" + "</p>";
            html += "<p id='mountains'>Mountains: " + mountains + "%" + "</p>";
            html += "<p id='sea'>Sea: " + sea + "%" + "</p>";
            html += "<p id='sunset'>Sunset: " + sunset + "%" + "</p>";
            html += "<p class='trees' id='trees'>Trees: " + trees + "%" + "</p>";
            html += "</div>"
            html += "</div>";
            $("#history").append(html);
            // myFunction();
        });

    console.log("Reply from server end");
};


function myFunction() {
    var c = $(".histresults").children;
    var i;
    for (i = 0; i < c.length; i++) {
        console.log(i);
        $a(".desert").style.backgroundColor = "pink";
        $a(".trees").style.backgroundColor = "pink";
    };
};



window.onload = function () {
    $a("#predict_button").onclick = predict;
};
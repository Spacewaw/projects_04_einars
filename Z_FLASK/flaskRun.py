from flask import Flask, request, jsonify
import base64
import numpy as np
import io
from PIL import Image
import keras
from keras import backend as K
from keras.models import Sequential , load_model
from keras.preprocessing.image import ImageDataGenerator, img_to_array
import matplotlib.pyplot as plt
app = Flask(__name__)

def get_model():
    global model_1
    global model_2
    global model_3
    global model_4
    global model_5
    #model = load_model('C:/Users/HOUSE/Music/Z_FLASK/my_model.h5')
    model_1 = load_model('C:/Users/HOUSE/Music/Z_FLASK/model_1.h5')
    model_2 = load_model('C:/Users/HOUSE/Music/Z_FLASK/model_2.h5')
    model_3 = load_model('C:/Users/HOUSE/Music/Z_FLASK/model_3.h5')
    model_4 = load_model('C:/Users/HOUSE/Music/Z_FLASK/model_4.h5')
    model_5 = load_model('C:/Users/HOUSE/Music/Z_FLASK/model_5.h5')
    #print(model.summary())
    print("MODELS ALL LOADED")

print("Loaidn Keras model")
get_model()

def preprocess_image_100_100(image, target_size):
    if image.mode != "RGB":
        image = image.convert("RGB")
    image = image.resize(target_size)
    image = img_to_array(image)
    #image = np.expand_dims(image, axis=0)
    image = image/255.0
    image = image.reshape(1, 100,100, 3)
    return image

def preprocess_image_200_200(image, target_size):
    if image.mode != "RGB":
        image = image.convert("RGB")
    image = image.resize(target_size)
    image = img_to_array(image)
    #image = np.expand_dims(image, axis=0)
    image = image/255.0
    image = image.reshape(1, 200,200, 3)
    return image


def preprocess_image_200_250(image, target_size):
    if image.mode != "RGB":
        image = image.convert("RGB")
    image = image.resize(target_size)
    image = img_to_array(image)
    #image = np.expand_dims(image, axis=0)
    image = image/255.0
    image = image.reshape(1, 200,250, 3)
    return image


@app.route('/hello', methods=['POST'])
def hello():
    message = request.get_json(force = True)
    name = message['name']
    response = {'DictKey':  'Hello, ' +  name + '!'}
    return jsonify(response)

@app.route("/predictModel1", methods=['POST'])
def predictM1():
    message = request.get_json(force=True)
    encoded = message['image']
    decoded = base64.b64decode(encoded)
    io1 = io.BytesIO(decoded)
    image = Image.open(io1)
    preprocessed_image = preprocess_image_100_100(image, target_size=(100,100))
    print(io1)
    print(image)
    print(preprocessed_image.shape)
    print(preprocessed_image.max())
    print(preprocessed_image.min())
    prediction = model_1.predict(preprocessed_image).tolist()
    #print(prediction.round(2))
    print(prediction[0][0])
    print(prediction[0][1])
    print(prediction[0][2])  
    print(prediction[0][3])
    print(prediction[0][4])
    response = {'prediction':{'desert': prediction[0][0],'mountains': prediction[0][1],
    'sea': prediction[0][2],'sunset': prediction[0][3],'trees': prediction[0][4] }}
    return jsonify(response)




@app.route("/predictModel2", methods=['POST'])
def predictM2():
    message = request.get_json(force=True)
    encoded = message['image']
    decoded = base64.b64decode(encoded)
    io1 = io.BytesIO(decoded)
    image = Image.open(io1)
    preprocessed_image = preprocess_image_100_100(image, target_size=(100,100))
    print(io1)
    print(image)
    print(preprocessed_image.shape)
    print(preprocessed_image.max())
    print(preprocessed_image.min())
    prediction = model_2.predict(preprocessed_image).tolist()
    #print(prediction.round(2))
    print(prediction[0][0])
    print(prediction[0][1])
    print(prediction[0][2])  
    print(prediction[0][3])
    print(prediction[0][4])
    response = {'prediction':{'desert': prediction[0][0],'mountains': prediction[0][1],
    'sea': prediction[0][2],'sunset': prediction[0][3],'trees': prediction[0][4] }}
    return jsonify(response)    



@app.route("/predictModel3", methods=['POST'])
def predictM3():
    message = request.get_json(force=True)
    encoded = message['image']
    decoded = base64.b64decode(encoded)
    io1 = io.BytesIO(decoded)
    image = Image.open(io1)
    preprocessed_image = preprocess_image_200_200(image, target_size=(200,200))
    print(io1)
    print(image)
    print(preprocessed_image.shape)
    print(preprocessed_image.max())
    print(preprocessed_image.min())
    prediction = model_3.predict(preprocessed_image).tolist()
    #print(prediction.round(2))
    print(prediction[0][0])
    print(prediction[0][1])
    print(prediction[0][2])  
    print(prediction[0][3])
    print(prediction[0][4])
    response = {'prediction':{'desert': prediction[0][0],'mountains': prediction[0][1],
    'sea': prediction[0][2],'sunset': prediction[0][3],'trees': prediction[0][4] }}
    return jsonify(response) 


@app.route("/predictModel4", methods=['POST'])
def predictM4():
    message = request.get_json(force=True)
    encoded = message['image']
    decoded = base64.b64decode(encoded)
    io1 = io.BytesIO(decoded)
    image = Image.open(io1)
    preprocessed_image = preprocess_image_200_200(image, target_size=(200,200))
    print(io1)
    print(image)
    print(preprocessed_image.shape)
    print(preprocessed_image.max())
    print(preprocessed_image.min())
    prediction = model_4.predict(preprocessed_image).tolist()
    #print(prediction.round(2))
    print(prediction[0][0])
    print(prediction[0][1])
    print(prediction[0][2])  
    print(prediction[0][3])
    print(prediction[0][4])
    response = {'prediction':{'desert': prediction[0][0],'mountains': prediction[0][1],
    'sea': prediction[0][2],'sunset': prediction[0][3],'trees': prediction[0][4] }}
    return jsonify(response) 

@app.route("/predictModel5", methods=['POST'])
def predictM5():
    message = request.get_json(force=True)
    encoded = message['image']
    decoded = base64.b64decode(encoded)
    io1 = io.BytesIO(decoded)
    image = Image.open(io1)
    preprocessed_image = preprocess_image_200_250(image, target_size=(200,250))
    print(io1)
    print(image)
    print(preprocessed_image.shape)
    print(preprocessed_image.max())
    print(preprocessed_image.min())
    prediction = model_5.predict(preprocessed_image).tolist()
    #print(prediction.round(2))
    print(prediction[0][0])
    print(prediction[0][1])
    print(prediction[0][2])  
    print(prediction[0][3])
    print(prediction[0][4])
    response = {'prediction':{'desert': prediction[0][0],'mountains': prediction[0][1],
    'sea': prediction[0][2],'sunset': prediction[0][3],'trees': prediction[0][4] }}
    return jsonify(response) 

    
if __name__ == '__main__':
    app.run(debug=True)  